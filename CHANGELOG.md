[0.1.0]
* Use OnlyOffice Enterprise Edition 6.4.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#640)

[0.2.0]
* Fixup spellchecker

[0.3.0]
* Update OnlyOffice to 7.0.0
* Update base image to 3.2.0

[1.0.0]
* Update OnlyOffice to 7.2.1

[1.0.1]
* Update OnlyOffice to 7.2.2

[1.1.0]
* Update OnlyOffice to 7.3.0

[1.1.1]
* Update OnlyOffice Enterprise to 7.3.2
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#732)
* Fix work of sessionIdle parameter (Bug #61049)
* Fix disconnect reason for socket.io
* Fix work of shutdown script (Bug #60982)
* Revert to classic socket.io upgrade to fix connection issue with proxy
* Fix database creation without onlyoffice owner (Bug #59826)
* Fix bug with double messages from editor

[1.2.0]
* Update OnlyOffice Enterprise to 7.4.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#741)

[1.3.0]
* Update OnlyOffice Enterprise to 7.5.0
* Update base image to 4.2.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#750)

[1.3.1]
* Update OnlyOffice Enterprise to 7.5.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#751)

[1.4.0]
* Update OnlyOffice Enterprise to 8.0.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#800)

[1.4.1]
* Update OnlyOffice Enterprise to 8.0.1
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#801)

[1.4.2]
* Fix custom fonts

[1.5.0]
* Update OnlyOffice Enterprise to 8.1.0
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#810)

[1.5.1]
* Update OnlyOffice Enterprise to 8.1.3
* [Full changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#813)

[1.6.0]
* Update DocumentServer to 8.2.0
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#813)
* Optimization of script loading to speed up opening all editors
* Own rendering of some SmartArt objects instead of recorded images in the file
* Updated dictionaries for Spellcheck and Hyphenation
* Support for new types of charts for opening: Histogram, Waterfall, and Funnel
* Improved display of Chart labels for Date axes, Values, and Categories
* Improved touch screen support in the full version of editors
* New Gray interface theme
* The appearance of the Version History panel has been changed
* Updated styles for file renaming
* Menu items in the File menu have been regrouped and icons have been added
* Theme and toolbar settings are moved to a separate Appearance category in the
* Added the Tab style setting - view of tabs (Fill or Line) in the File menu -
* Added the Use toolbar color as tabs background setting
* Ability to add custom fields to the file information
* Ability to view/add/edit custom fields without pressing the Apply button
* Reorganized fields with file information in the File menu - Info
* Improved work with the Lists of languages - the name of the language
* Implemented search in the Language list by name and in the target language,
* Support for old CheckBox types
* Added the ability to Insert the contents of a third-party document
* New numbered list presets for Arabic interface
* Added the highlight of deleted text in the selected file version
* Ability to add and edit complex fields using field instructions
* Improved support for smooth scroll
* Support for iterative calculations
* Added the ability to switch the direction of cell placement
* Added a button for setting the number format with separators to the toolbar
* Ability to display pages in real scale in the Print preview window
* The Pivot Table toolbar tab is only displayed when using the pivot table,
* The Pivot Table settings right panel now unfolds when adding
* Added the ability to display trendlines (Equation on chart) to the Chart
* Acceleration of opening files due to rendering the slide before loading
* Implemented a more visual way of selecting animation from the extended list
* Added the `Random` transition
* Added a Signature field with the ability to upload a prepared image
* Added ability to save `PDFs` to the storage
* Added co-editing `PDFs`
* Added correct processing of cropped/combined shapes when opening files
* Added Gradient support
* Improved text recognition
* Fixed the XSS injection in the Tooltip of the Animation Pane
* Fixed the vulnerability that allowed bypassing document access rights
* Fixed the vulnerabilities in the `FormattedDiskPagePAPX`,
* Fixed the vulnerabilities in the `ECMADecryptor`, and `DirTree`
* Ability to send email notifications about warnings related to license
* Added cache reset when regenerating fonts, in the path to the scripts,
* Added the `mysqlExtraOptions` object with the ability to pass additional
* Added the `servicesCoAuthoring.server.forceSaveUsingButtonWithoutChanges`
* Added the `users` parameter, similar to `users` in [callback handler](https://api.onlyoffice.com/editors/callback#users)
* Added the `Password` and `PasswordToOpen` parameters to the `convert-to`
* Extend support for the
* Added parameters for toolbar appearance configuration:
* Common logo for all themes can be specified in the `customization.logo.image` parameter
* Support for the `customization.toolbarNoTabs` parameter has been
* Added methods for implementing Undo/Redo through plugins
* Added processing of the `standardView` parameters for displaying the editor
* Added support for custom functions based on `jsdoc`
* Commercial editions now require the installation of plugins via
* The Macros button has been moved to the View tab

[1.6.1]
* Update DocumentServer to 8.2.1
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#813)
* Fixed appearance of an extra argument when switching from a linear equation
* Fixed navigation via menu instead of moving around text when using
* Fixed inconsistency in the appearance of labels as compared to other editors
* Fixed the ability to add some `TIF`/`TIFF` images to documents
* Fixed an issue with calculating spacing before for a paragraph
* Fixed an issue with cell selection after removing table rows
* Fixed calculating footnotes in the extreme case when there is no space even
* Fixed an issue with the exact row height support when opening `DOCX` files
* Fixed stopping work of the editors when updating the editor without saving
* Fixed stopping work of the editor when moving the magnifying glass tool
* Fixed the input window overlay on text in the Japanese (Microsoft IME) layout
* Fixed inconsistency in displaying numbered lists as compared to other editors
* ...

[1.6.2]
* Update DocumentServer to 8.2.2
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#813)
* Fixed the ability to read out a document in the Firefox browser using
* Fixed displaying some chart types when opening the `DOCX` or `PPTX` documents
* Fixed availability of buttons on the right panel when the zoom is higher
* Fixed stopping work of the editor when working with the Text box
* Fixed slow scrolling of documents if the document extends the visible area
* Fixed stopping work of the editor when comparing some `DOCX` documents
* Fixed text selection when clicking on a paragraph with Justified
* Fixed stopping work of the editor when deleting a formula from a cell
* Fixed disappearing formulas when opening some `XLSX` files

[1.7.0]
* Update DocumentServer to 8.3.0
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#813)
* Added support for logical operations on shapes (unite, combine, fragment,
* Added the Opacity option for images
* Added the ability to reset Crop for images in the right panel
* Added the interface translation into Albanian (sq-AL, Albanian (Albania))
* Added new languages to the list of exceptions for text AutoCorrect
* Implemented RTL interface support for the embedded viewer
* Blocking dialogs that require the editor restart are replaced
* Connection status notifications are moved from the status bar to the top
* Implemented support for `customXml` and `dataBinding`, when the content
* When editing protected documents, the tools available for the selected part
* Added the ability to change the main direction of the paragraph
* Add the support of the HWP and HWPX formats for opening in the editors
* Added support for opening the Pages (.pages) file format

[1.7.1]
* Update DocumentServer to 8.3.1
* [Full Changelog](https://github.com/ONLYOFFICE/DocumentServer/blob/master/CHANGELOG.md#813)
* Added the ability to move pages in `PDF` files ([DocumentServer#1749](https://github.com/ONLYOFFICE/DocumentServer/issues/3052))
* Added the parameter which hides tips about new features
* Fixed stopping work of the editor when pasting a large amount of copied data
* Fixed crash after entering the minus sign after the equation
* Fixed crash after entering a character before the division sign
* Fixed stopping work of the editor when building some `DOCX` files where
* Fixed an issue with undone changes when turning off and on the "Show changes
* Fixed crash of the editor when pasting text in the Track Changes mode in some
* Fixed stopping work of the editor when removing a paragraph
* Fixed reset of the Text direction > RTL option when placing the cursor
* Fixed incorrect display of characters when entering text in RTL before saving

